package ejercicios_complementarios;

import java.util.*;

public class Juego_Barcos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[] vector = { "agua", "agua", "agua", "agua", "agua", "agua", "agua", "agua", "agua", "agua" };

		int posicion = (int) (Math.round(Math.random() * 10));

		vector[posicion] = "barco";

		Scanner entrada = new Scanner(System.in);

		System.out.println("Introduce la posicion en la que crees que se encuentra el barco(0-9:");

		int respuesta = entrada.nextInt();

		int contador = 0;

		while (vector[respuesta] != "barco") {
			
			contador++;

			System.out.println("!AGUA! Intentalo de nuevo: ");
			
			respuesta = entrada.nextInt();
			
		}

		System.out.println("!BARCO HUNDIDO!"+" Has acertado en "+(contador+1)+" intentos");

	}

}
