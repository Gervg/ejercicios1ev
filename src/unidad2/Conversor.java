package unidad2;

import javax.swing.JOptionPane;

public class Conversor {

	public static void main(String[] args) {
		
		double euros = Double.parseDouble(JOptionPane.showInputDialog("Introduce un número: "));
		double valorCambio = 1.18;
		double resultado = euros * valorCambio;
		
		System.out.printf("El valor de  " + euros + " euros" + " en dolares es: %1.2f dolares", resultado); 
		
		
	}

}
