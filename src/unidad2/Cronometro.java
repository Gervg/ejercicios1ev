package unidad2;

import javax.swing.*;

import java.util.*;

public class Cronometro {
	
	public static void main(String[] args) {
		
		Calendar ahora1 = Calendar.getInstance();
		long tiempo1 = ahora1.getTimeInMillis();
		
		String nombre = JOptionPane.showInputDialog("Dime tu nombre: ");
		
		Calendar ahora2 = Calendar.getInstance();
		long tiempo2 = ahora2.getTimeInMillis();
		
		long diferencia = tiempo2 - tiempo1;
		
		System.out.printf("Hola, " + nombre + " has tardado: %.3f segundos en decirme tu nombre",(double)diferencia/1000);
			
	}

}
