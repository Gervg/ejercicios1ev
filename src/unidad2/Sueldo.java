package unidad2;

import java.util.*;

public class Sueldo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Introduce tu sueldo base: ");
		
		double sueldo_base = entrada.nextDouble();
		
		System.out.println("Introduce el importe de la primera venta: ");
		
		double venta1 = entrada.nextDouble();
		
		System.out.println("Introduce el importe de la segunda venta: ");
		
		double venta2 = entrada.nextDouble();
		
		System.out.println("Introduce el importe de la tercera venta: ");
		
		double venta3 = entrada.nextDouble();
		
		double comision = (venta1 + venta2 + venta3)*0.1;
		
		double sueldo_total = sueldo_base + comision;
		
		System.out.printf("Tu concepto en comisiones es: %1.2f€\n", comision);
		
		System.out.printf("Tu sueldo total es : %1.2f€", sueldo_total);
	}

}
