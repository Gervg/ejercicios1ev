package unidad3;

import java.util.*;

public class Adivinanumero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	int aleatorio = (int)(Math.random()*1000);
		
		Scanner entrada = new Scanner(System.in);
		
		int numero = 0;
		
		System.out.println("He pensado un número entre 1 y 1000, intenta adivinarlo");
		
		do {
			
			numero = entrada.nextInt();
			
			if(numero > aleatorio) {
				
				System.out.println("El número es más menor que " + numero);
			}
			
			else if(numero < aleatorio) {
				
				System.out.println("El número es mayor que " + numero);	
			}
			
			if(numero != aleatorio)
			
			System.out.println("Intentalo de nuevo");
			
		} while(numero != aleatorio);
		
		System.out.println("Correcto, lo has conseguido");
	
	}

}
