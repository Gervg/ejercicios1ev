package unidad3;

import java.util.*;

public class Bisiesto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Introduce un año: ");
		
		int año = entrada.nextInt();
		
		if((año %4 == 0) && ((año %100 != 0) || (año %400 ==0))){
			
		System.out.println("El año " + año + " es bisisesto");
		
		}
		
		else 
			
		System.out.println("El año " + año + " no es bisiesto");
		
	}

}
