package unidad3;

import java.util.*;

public class Calculadora {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double numero01, numero02, resultado = 0;
		
		String sumar, restar, multiplicar, dividir;
		
		String respuesta = null;
		
		sumar = "+";
		
		restar = "-";
		
		multiplicar = "*";
		
		dividir = "/";
		
		Scanner entrada = new Scanner(System.in);
	
		System.out.println("Indique que operación desea realizar,si quiere salir del programa sin realizar ninguna escriba no: ");
		
		respuesta = entrada.next();
		
		while(respuesta.equalsIgnoreCase(sumar) || respuesta.equalsIgnoreCase(restar) ||respuesta.equalsIgnoreCase(dividir) || respuesta.equalsIgnoreCase(multiplicar)) {
			
			System.out.println("Intrpduzca el primer numero: ");
			
			numero01 = entrada.nextDouble();
			
			System.out.println("Introduzca el segundo numero: ");
			
			numero02 = entrada.nextDouble();
			
			if((respuesta.equalsIgnoreCase(sumar))){
				
				 resultado = numero01 + numero02;
				
			}
			
			if(respuesta.equalsIgnoreCase(restar)) {
				
				 resultado = numero01 - numero02;
			}
			
			if(respuesta.equalsIgnoreCase(dividir)) {
				
				resultado = numero01 / numero02;
			}
			
			if(respuesta.equalsIgnoreCase(multiplicar)) {
				
				respuesta = "x";
				
				resultado = numero01 * numero02;
			}
			
			System.out.printf(numero01 + " " + respuesta + " " + numero02 + " " + "= %1.2f\n", resultado);
			
			System.out.println("¿Quiere realizar otra operación?(Introduzca la operacion a realizar o la palabra: no)");
			
			respuesta = entrada.next();
		}
		
	}

}
