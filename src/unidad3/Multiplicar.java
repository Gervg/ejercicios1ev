package unidad3;

import java.util.*;

public class Multiplicar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner entrada = new Scanner(System.in);
	
		int error = 0;
		
		String respuesta1;
		
		String respuesta2 = "si";

			
		do {	
			
			System.out.println("¿Que tabla quieres repasar(1/9)?: ");
			
			int tabla = entrada.nextInt();
			
			for(int i = 1; i < 11; i++) {
				
				int resultado = tabla * i;
				
				System.out.println("¿Cual es el resultado de " + tabla + " x " + i + "?");
				
				int respuesta = entrada.nextInt();
				
				if(respuesta == resultado) {
					
					System.out.println("Respuesta correcta");
				}
				else {
					
					System.out.println("Respuesta incorrecta. La respuesta correcta es: " + resultado);
					
					error++;
					
				}
	
			}
			
			if(error < 3) {
				
				System.out.println("Has aprobado");
				
			}
			else {
		
				System.out.println("Has suspendido");
				
			}
			
			System.out.println("¿Quieres repasar otra tabla(si/no)?");
			
			respuesta1 = entrada.next();
			
		}while(respuesta1.equalsIgnoreCase(respuesta2));
			
	}
	
}


		

	
