package unidad3;

import java.util.Scanner;

public class ParImpar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Introduce un número: ");
		
		int numero = entrada.nextInt();
		
		if(numero %2 == 0) {
			
			System.out.println("El " + numero + " es par.");
			
		}
		
		else {
			
			System.out.println("El " + numero + " es impar");
		}
	}

}
