package unidad3;

import java.util.*;

public class Triangulo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Introduce la longitud del primer lado: ");
		
		double lado1 = entrada.nextDouble();
		
		System.out.println("Introduce la longitud del segundo lado: ");
		
		double lado2 = entrada.nextDouble();
		
		System.out.println("Introduce la longitud del tercer lado: ");
		
		double lado3 = entrada.nextDouble();
		
		if(lado1 == lado2 && lado2 == lado3)
				
		System.out.println("El triangulo es equilatero");
		
		else
		{
			if(lado1 == lado2 || lado1 == lado3 || lado2 == lado3)
				
			System.out.println("El triangulo es isosceles");
			
			else
				
			System.out.println("El triangulo es escaleno");
			
		}
	}

}
