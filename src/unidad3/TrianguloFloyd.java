package unidad3;

import java.util.*;

public class TrianguloFloyd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int filas, numero = 1;
		
		System.out.print("Introduce el número de líneas del triángulo de Floyd: ");
		
		Scanner entrada = new Scanner(System.in);
		
		filas = entrada.nextInt();
		
		for(int i=1; i<=filas; i++){
			
			for(int j=1; j<=i; j++){
				
				System.out.print("\t" + numero);
				numero++;
			}
			
			System.out.println();
			
		}
		
		
	}

}
