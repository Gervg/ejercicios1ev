package unidad4;

import java.util.*;

public class Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);
		
		int vocales=0;
		
		System.out.println("Introduce una frase por teclado: ");
		
		String frase = entrada.nextLine();
		
		for(int g=0 ; g < frase.length(); g++ ) {
			
			if(frase.charAt(g) == 'a' | frase.charAt(g) == 'e' | frase.charAt(g) == 'i' | frase.charAt(g) ==  'o' | frase.charAt(g) == 'u') {
				
				vocales++;
				
			}
			
		}
		
		System.out.println("La frase que has introducido contine " + vocales + " vocales");
		
	}

}
