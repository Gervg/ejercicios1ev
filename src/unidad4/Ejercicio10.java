package unidad4;

import java.util.*;

public class Ejercicio10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);

		System.out.println("Introduce el numero de valores totales (entre 10 y 20)");

		int n = entrada.nextInt();

		while (n < 10 || n > 20) {

			System.out.println("Introduce un valor valido");
			
			n = entrada.nextInt();

		}

		int[] vector = new int[n];

		for (int i = 0; i < vector.length; i++) {

			vector[i] = (int) (Math.round(Math.random() * 200) - 100);

			System.out.print(vector[i] + "; ");
		}
		
		int valor=200, valorf;
		
		for(int j=1; j<vector.length; j++) {
			
			valorf = vector[j]-vector[j-1];
			
			if(valorf < valor ) {
				
				valor = valorf;
			}
		}
		
		System.out.println();
		
		System.out.println("La diferencia minima entre dos valores adyacentes es:" + valor);

		

	}

}
