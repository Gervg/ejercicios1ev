package unidad4;

import java.util.*;

public class Ejercicio11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[] vector = new String[10];

		Scanner entrada = new Scanner(System.in);

		String nombre = "";

		for (int i = 0; i < vector.length; i++) {

			System.out.println("Introduce el nombre " + (i + 1) + ":");

			nombre = entrada.next();

			vector[i] = nombre;
		}

		System.out.println("La cadena mas larga es: " + cadenamaslarga(vector));

	}

	public static String cadenamaslarga(String[] vector) {

		String cadena = vector[0];

		for (int j = 1; j < vector.length; j++) {

			if (vector[j].length() > cadena.length()) {

				cadena = vector[j];
			}
		}

		return cadena;

	}

}
