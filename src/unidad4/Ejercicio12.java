package unidad4;

import java.util.*;

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);
		
		int[] vector = new int[(int) (Math.round(Math.random() * 11) + 1)];
		
		Integer c = centro(vector);

		for (int i = 0; i < vector.length; i++) {

			System.out.println("Introduce el valor del indice " + (i + 1) + ":");

			int valor = entrada.nextInt();

			vector[i] = valor;
		}
		
		System.out.println(c!=null ? ("El centro del vector esta en la posicion: " + c): "No tiene centro");

	}

	public static Integer centro(int[] vector) {

		int c = 0;

		int izquierda;

		int derecha;

		do {

			c++;

			izquierda = derecha = 0;

			for (int j = 1; j < c; j++) {

				izquierda = izquierda + (c - j) * vector[j];

			}

			for (int z = 1; z < vector.length; z++) {

				derecha = derecha + (c - z) * vector[z];
			}

		} while (izquierda != derecha && c <= vector.length - 1);

		if (c == vector.length - 1) {

			return null;
		} else {

			return c;
		}
	}

}
