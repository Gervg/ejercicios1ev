package unidad4;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);

		System.out.println("Introduce el núemero de secuencias: ");

		int secuencias = entrada.nextInt();

		int valores = (int) (Math.round(Math.random() * 9) + 1);

		int[][] matriz = new int[secuencias][valores];

		for (int m = 0; m < secuencias; m++) {

			for (int n = 0; n < valores; n++) {

				matriz[m][n] = (int) (Math.round(Math.random() * 99) + 1);
			}
		}

		int contador = 0;

		for (int i = 0; i < secuencias; i++) {

			contador++;

			System.out.println();

			System.out.println("La secuencia " + contador + " contiene: " + valores + " valores\n");

			for (int j = 0; j < valores; j++) {

				System.out.print(matriz[i][j] + " ");
			}
		}

	}

}
