package unidad4;

import java.util.Arrays;

public class Ejercicio17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] vector1 = new int[(int) (Math.round(Math.random() * 90) + 10)];
		int[] vector2 = new int[(int) (Math.round(Math.random() * 90) + 10)];

		for (int i = 0; i < vector1.length; i++) {

			vector1[i] = (int) (Math.round(Math.random() * 200) - 100);
		}

		for (int j = 0; j < vector2.length; j++) {

			vector2[j] = (int) (Math.round(Math.random() * 200) - 100);
		}

		Arrays.sort(vector1);

		Arrays.sort(vector2);

		int[] vector3 = new int[vector1.length + vector2.length];

		System.arraycopy(vector1, 0, vector3, 0, vector1.length);

		System.arraycopy(vector2, 0, vector3, vector1.length, vector2.length);

		Arrays.sort(vector3);

		System.out.println("Vector 1 orenado:");

		for (int z = 0; z < vector1.length; z++) {

			System.out.print(vector1[z] + " ");
		}

		System.out.println();

		System.out.println("Vector 2 ordenado:");

		for (int y = 0; y < vector2.length; y++) {

			System.out.print(vector2[y] + " ");
		}

		System.out.println();

		System.out.println("Vector 3 ordenado:");

		for (int x = 0; x < vector3.length; x++) {

			System.out.print(vector3[x] + " ");
		}

	}

}
