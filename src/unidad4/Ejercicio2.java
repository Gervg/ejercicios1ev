package unidad4;

import java.util.*;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Introduce un frase: ");
		
		String frase = entrada.nextLine();
		
		StringBuilder builder = new StringBuilder(frase);
		
		String fraseinvertida = builder.reverse().toString();
		
		System.out.println(fraseinvertida);
		
	}

}
