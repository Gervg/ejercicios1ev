package unidad4;

import java.util.*;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner entrada = new Scanner(System.in);
		
		int repeticion = 0;
		
		System.out.println("Introduce una cadena de caracteres: ");
		
		String cad1 = entrada.nextLine();
		
		System.out.println("Introduce una segunda cadena de caracteres: ");
		
		String cad2 = entrada.nextLine();
		
		for(int i=0 ; i < cad1.length(); i++) {
			
			for(int j=0 ; j < cad2.length() ; j++) {
				
				if(i==j) {
					
					repeticion++;
				}
			}
		}
		
		System.out.println("La segunda cadena esta contenida " + repeticion + " veces en la primera");

	}

}
