package unidad4;

import java.util.*;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);
		
		System.out.println("¿Cuantas veces quieres lanzar el dado?: ");
		
		int numero = entrada.nextInt();
		
		int cara1=0, cara2=0, cara3=0, cara4=0, cara5=0, cara6=0;
		
		for(int i=1 ; i <= numero; i++) {
			
			int valorDado = (int) Math.floor(Math.random()*6+1);
			
			switch(valorDado) {
			
			case 1:
				cara1++;
				break;
				
			case 2:
				cara2++;
				break;
				
			case 3:
				cara3++;
				break;
				
			case 4:
				cara4++;
				break;
				
			case 5:
				cara5++;
				break;
				
			case 6:
				cara6++;
				break;
				
			}
		}
		
		System.out.println("La cara del 1 ha salido " + cara1 + " veces.");
		System.out.println("La cara del 2 ha salido " + cara2 + " veces.");
		System.out.println("La cara del 3 ha salido " + cara3 + " veces.");
		System.out.println("La cara del 4 ha salido " + cara4 + " veces.");
		System.out.println("La cara del 5 ha salido " + cara5 + " veces.");
		System.out.println("La cara del 6 ha salido " + cara6 + " veces.");
		
	}

}
