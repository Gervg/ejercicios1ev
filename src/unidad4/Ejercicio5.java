package unidad4;

import java.util.*;

public class Ejercicio5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner entrada = new Scanner(System.in);
		
		String letras ="TRWAGMYFPDXBNJZSQVHLCKE";
		
		System.out.println("Introduce un dni completo: ");
		
		String dni = entrada.nextLine();
		
		int numero = Integer.parseInt(dni.substring(0, dni.length() - 1));
		
		int resto = numero % 23;
		
		char letra = dni.charAt(dni.length() - 1);
		
		if(letra == letras.charAt(resto)) {
			
			System.out.println("El dni es correcto");
		}else {
			
			System.out.println("El dni es incorrecto");
		}
		
		

	}

}
