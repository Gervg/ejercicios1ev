package unidad4;

public class Ejercicio9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int vector[] = new int[(int) (Math.round(Math.random() * 490) + 10)];

		for (int i = 0; i < vector.length; i++) {

			vector[i] = (int) (Math.round(Math.random() * 200) - 100);

			if (vector.length <= 100) {

				System.out.print(vector[i] + " ,");

			}
		}
		
		int secuencias=0;
		
		boolean detectada = false;
		
		for(int j=1; j<vector.length; j++) {
			
			if(vector[j] == vector[j-1]) {
				
				if(!detectada) {
					
					detectada=true;
					
					secuencias++;
				}
				
				else if(detectada) {
					
					detectada = false;
				}
				
				
			}
		}
		
		System.out.println("numero de secuencias: " + secuencias);

	}

}
